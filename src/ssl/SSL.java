/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssl;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import ui.Main;

/**
 *
 * @author salomodn
 */
public class SSL {

    public static void main(String[] args) {
       
        try {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                /* Create and display the form */
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        Main main = new Main();
                        main.setTitle("Self Signed Certificate Generator");
                        main.setVisible(true);
                    } 
                });
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (UnsupportedLookAndFeelException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }
}
