package util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.PaintContext;
import java.awt.RenderingHints;
import java.awt.event.ItemEvent;
import java.awt.image.ColorModel;
import java.awt.image.Raster;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import ui.Main;

public class BasicComboBoxEditorHint extends JTextField implements AutoCompleteComboBox.StateListener{
    

    private Icon icon;
    private String hint;
    private Insets dummyInsets;

    public BasicComboBoxEditorHint(String icon) {
        //setIcon(createImageIcon("/images/" + icon + ".png", icon));
        setIcon(createImageIcon("/images/countries/" + icon + ".png", "country"));
        AutoCompleteComboBox.setStateListener(this);

    }

    public void setIcon(Icon newIcon) {
        this.icon = newIcon;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        int textX = 2;

        if (this.icon != null) {
            int iconWidth = icon.getIconWidth();
            int iconHeight = icon.getIconHeight();
            int x = 5 + 5;
            textX = x + iconWidth + 2;
            int y = (this.getHeight() - iconHeight) / 2;
            icon.paintIcon(this, g, x, y);
        }

        setMargin(new Insets(2, textX, 2, 2));

        if (this.getText().toLowerCase().equals("") &&this.getText().length()>0) {
            int width = this.getWidth();
            int height = this.getHeight();
            Font prev = g.getFont();
            Font italic = prev.deriveFont(Font.ITALIC);
            Color prevColor = g.getColor();
            g.setFont(italic);
            g.setColor(UIManager.getColor("textInactiveText"));
            int h = g.getFontMetrics().getHeight();
            int textBottom = (height - h) / 2 + h - 4;
            int x = this.getInsets().left;
            Graphics2D g2d = (Graphics2D) g;
            RenderingHints hints = g2d.getRenderingHints();
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2d.drawString(hint, x, textBottom);
            g2d.setRenderingHints(hints);
            g.setFont(prev);
            g.setColor(prevColor);
        }

    }

    protected ImageIcon createImageIcon(String path, String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return new ImageIcon("/images/" + "country" + ".png", description);
        }
    }

    @Override
    public void changed(ItemEvent e) {
        String country = e.getItem().toString();
        //System.out.println("Country: "+country);
        //setIcon(new ImageIcon(getClass().getResource("/images/countries/"+country)));
        setIcon(createImageIcon("/images/countries/"+country+".png", country));
        
    }

    

}
