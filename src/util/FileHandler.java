/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.System.in;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import ui.Test;

/**
 *
 * @author solomonkinu
 */
public class FileHandler {

    File file;

    public FileHandler() {
    }

    public FileHandler(File file) {
        this.file = file;
    }

    public String getPath() {
        return file.getAbsoluteFile().getParent().concat("/");
    }

    public String getFileName() {
        String parentPath = file.getAbsoluteFile().getParent().concat("/").concat(file.getName());
        int p = parentPath.lastIndexOf(".");
        if (p < 0) {
            return parentPath;
        }

        int d = parentPath.lastIndexOf(File.separator);

        if (d < 0 && p == 0) {
            return parentPath;
        }

        if (d >= 0 && d > p) {
            return parentPath;
        }

        return parentPath.substring(0, p);
    }

    public String fileAs(String extension) {
        return getFileName().concat(extension);
    }

    private List<String> getCountries1() {
        List<String> lines = Collections.emptyList();
        try {
            lines = Files.readAllLines(Paths.get(getClass().getResource("/countries.txt").toURI()));
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lines;
    }

    private List<String> getCountries0() {
        List<String> countries = new ArrayList();
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("/images/countries");
        System.out.println(getClass().getName());
        if (getClass().getName() == null) {
            throw new IllegalArgumentException("file not found! " + "/images/countries");
        } else {
            File folder = new File(getClass().getResource("/images/countries").getPath());
            System.out.println(folder);
            File[] listOfFiles = folder.listFiles();

            for (File file : listOfFiles) {
                if (file.isFile()) {
                    int p = file.getName().lastIndexOf(".");

                    String country = file.getName().substring(0, p);
                    //System.out.println(country);
                    countries.add(country);
                    Collections.sort(countries);
                    //System.out.println(file.getName());
                }
            }
            // failed if files have whitespaces or special characters
            //return new File(resource.getFile());

            //return new File(resource.toURI());
        }

        return countries;
    }

    public List<String> getCountries() {
        List<String> countries = Collections.emptyList();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/countries.txt")));
            StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                countries.add(line);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countries;
    }

    public void writeToFile(byte[] data, String path) {
        String str = path;//destination + "/" + file;
        try {
            FileOutputStream out = new FileOutputStream(str);
            out.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
