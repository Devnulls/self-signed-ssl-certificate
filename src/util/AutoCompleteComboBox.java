/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.basic.BasicComboBoxEditor;

/**
 *
 * @author salomodn
 */
public class AutoCompleteComboBox extends JComboBox {

    public int caretPos = 0;
    public JTextField inputField = null;

    private BasicComboBoxEditor basicComboBoxEditor;

    static StateListener stateListener;

    public static interface StateListener {

        void changed(java.awt.event.ItemEvent e);
    }

    public static void setStateListener(StateListener stateListener) {
        AutoCompleteComboBox.stateListener = stateListener;
    }

    public AutoCompleteComboBox(final Object elements[]) {
        super(elements);
        //setEditor(new BasicComboBoxEditor());
        basicComboBoxEditor = new BasicComboBoxEditor() {
            @Override
            protected JTextField createEditorComponent() {
                return new util.BasicComboBoxEditorHint((String) elements[0]);
            }
        };
        setEditor(basicComboBoxEditor);
        setEditable(true);
        ComboBoxRenderer comboBoxRenderer = new ComboBoxRenderer();
        setRenderer(comboBoxRenderer);
        this.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                stateListener.changed(evt);
            }
        });

    }

    public void setSelectedIndex(int index) {
        super.setSelectedIndex(index);

        inputField.setText(getItemAt(index).toString());
        inputField.setSelectionEnd(caretPos + inputField.getText().length());
        inputField.moveCaretPosition(caretPos);
    }

    public void setEditor(ComboBoxEditor editor) {
        super.setEditor(editor);
        if (editor.getEditorComponent() instanceof JTextField) {
            inputField = (JTextField) editor.getEditorComponent();

            inputField.addKeyListener(new KeyAdapter() {
                public void keyReleased(KeyEvent ev) {
                    char key = ev.getKeyChar();
                    if (!(Character.isLetterOrDigit(key) || Character.isSpaceChar(key))) {
                        return;
                    }

                    caretPos = inputField.getCaretPosition();
                    String text = "";
                    try {
                        text = inputField.getText(0, caretPos);
                        text = text.substring(0, 1).toUpperCase() + text.substring(1);// first character capital
                    } catch (javax.swing.text.BadLocationException e) {
                        e.printStackTrace();
                    }

                    for (int i = 0; i < getItemCount(); i++) {
                        String element = (String) getItemAt(i);
                        if (element.startsWith(text)) {
                            setSelectedIndex(i);
                            return;
                        }
                    }
                }
            });
        }
    }

//    @Override
//    public void itemStateChanged(ItemEvent e) {
//        if (e.getStateChange() == ItemEvent.SELECTED) {
//            //setEditor(new BasicComboBoxEditor());
//        }
//            //OutStrng += "Selected: " + (String)e1.getItem();
//        //else
//        //OutStrng += "DeSelected: " + (String)e1.getItem();    
//        
//    }
    class ComboBoxRenderer extends JLabel
            implements ListCellRenderer {

        public Component getListCellRendererComponent(
                JList list,
                Object value,
                int index,
                boolean isSelected,
                boolean cellHasFocus) {
            setText(value.toString());

            ImageIcon icon = null;
            try {
                icon = createImageIcon("/images/countries/" + value.toString() + ".png", "country");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            setIcon(icon);
            return this;
        }
    }

    protected ImageIcon createImageIcon(String path, String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
            //return new ImageIcon("/images/" + "country" + ".png", description);
        }
    }

}
