/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedOutputStream;
import ssl.*;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.xml.bind.DatatypeConverter;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Extensions;

import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import sun.security.util.DerOutputStream;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

/**
 *
 * @author salomodn
 */
public class SSLGenerator {

    private String certificateAlias = "YOUR_CERTIFICATE_NAME";
    private String cetificateAlgorithm = "RSA";
    private String certificateDN = "CN=cn, O=o, L=L, ST=il, C= c";
    private String certificateName = "keystore.jks";
    private String email = "";
    private String password;
    private String destination = "";
    private int certificateBits = 1024;
    Date validity;

    static {
        // adds the Bouncy castle provider to java security
        Security.addProvider(new BouncyCastleProvider());
    }

    public SSLGenerator() {
    }

    public void setCertificateAlias(String certificateAlias) {
        this.certificateAlias = certificateAlias;
    }

    public void setCertificateDN(String certificateDN) {
        this.certificateDN = certificateDN;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setValidity(Date validity) {
        this.validity = validity;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        SSLGenerator signedCertificate = new SSLGenerator();
        signedCertificate.createCertificate();
    }

    @SuppressWarnings("deprecation")
    public X509Certificate createCertificate(){
        X509Certificate cert = null;
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(cetificateAlgorithm);
            keyPairGenerator.initialize(certificateBits, new SecureRandom());
            KeyPair keyPair = keyPairGenerator.generateKeyPair();

            // GENERATE THE X509 CERTIFICATE
            X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
            v3CertGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
            v3CertGen.setIssuerDN(new X509Principal(certificateDN));
            v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24));
            v3CertGen.setNotAfter(validity);//new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 365 * 10))
            v3CertGen.setSubjectDN(new X509Principal(certificateDN));
            v3CertGen.setPublicKey(keyPair.getPublic());
            v3CertGen.setSignatureAlgorithm("SHA256WithRSAEncryption");
            cert = v3CertGen.generateX509Certificate(keyPair.getPrivate());
            v3CertGen.addExtension(X509Extensions.SubjectAlternativeName, false, new GeneralNames(
                    new GeneralName(GeneralName.rfc822Name, email)));
            saveCert(cert, keyPair);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cert;
    }

    private void saveCert(X509Certificate cert, KeyPair keyPair) throws Exception {
        KeyStore keyStore = KeyStore.getInstance("JKS");
        //
        //keyStore.setKeyEntry(certificateAlias, key, password.toCharArray(), new java.security.cert.Certificate[]{cert});
        
        
        File file = new File(destination, certificateName + ".jks"); 
        keyStore.load(null, null);
        if(password!=null){
            keyStore.setKeyEntry(certificateAlias, keyPair.getPrivate(), password.toCharArray(), new java.security.cert.Certificate[]{cert});
            keyStore.store(new FileOutputStream(file), password.toCharArray());
        }else{
           
            keyStore.setCertificateEntry(certificateAlias, cert);
            keyStore.store(new FileOutputStream(file), new char[0]);
        }
        

        /*========================================================================================*/
        /*========================================================================================*/
        //System.out.println("\n--------SAVING PUBLIC KEY AND PRIVATE KEY TO FILES-------\n");
        //RSA rsaObj = new RSA();
        //Pullingout parameters which makes up Key
        //System.out.println("\n------- PULLING OUT PARAMETERS WHICH MAKES KEYPAIR----------\n");
        
        //KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        //KeyPair keyPair = generateKeyPair();//RSA.createKeystore(file.getAbsolutePath(), certOwner, validity, alias, password);
        //RSAPublicKeySpec rsaPubKeySpec = keyFactory.getKeySpec(keyPair.getPublic(), RSAPublicKeySpec.class);
        //RSAPrivateKeySpec rsaPrivKeySpec = keyFactory.getKeySpec(keyPair.getPrivate(), RSAPrivateKeySpec.class);
        //File file2 = new File(file.getAbsolutePath());
        String parentPath = file.getAbsoluteFile().getParent() + "/";
        String PUBLIC_KEY_FILE = new FileHandler(file).fileAs("_public_.key");//removeExtension(parentPath + (file.getName())) + "_public_.key";
        String PRIVATE_KEY_FILE = new FileHandler(file).fileAs("_private_.key");//removeExtension(parentPath + file.getName()) + "_private_.key";

        //System.out.println("------------------------- PUBLIC_KEY_FILE ====" + PUBLIC_KEY_FILE);
        
        /*saveKeys(PUBLIC_KEY_FILE, rsaPubKeySpec.getModulus(), rsaPubKeySpec.getPublicExponent());
        saveKeys(PRIVATE_KEY_FILE, rsaPrivKeySpec.getModulus(), rsaPrivKeySpec.getPrivateExponent());*/

        //Generate CSR
        generateCSR(keyPair);
        // Generate Cert
        String CRT_KEY_FILE = new FileHandler(file).fileAs(".crt");//removeExtension(parentPath + file.getName()) + ".crt";
        certToString(cert, CRT_KEY_FILE);// crt file
        keyToString(keyPair, PRIVATE_KEY_FILE);// private key file
        keyToString(keyPair, PUBLIC_KEY_FILE);// public key file
        /*
        //converting public key to byte            
        byte[] byte_pubkey = keyPair.getPublic().getEncoded();
        System.out.println("\nBYTE KEY::: " + byte_pubkey);

        //converting byte to String 
        String str_key = Base64.getEncoder().encodeToString(byte_pubkey);
        // String str_key = new String(byte_pubkey,Charset.);
        System.out.println("\nSTRING KEY::" + str_key);

        //converting string to Bytes
        byte_pubkey  = Base64.getDecoder().decode(str_key);
        System.out.println("BYTE KEY::" + byte_pubkey);*/
        
        //JOptionPane.showMessageDialog(null, "SSL Certificates Generated", "System Message", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/images/info-36.png")));
        showMessage();
    }

    private void generateCSR(KeyPair keypair) {
        try {
            // create Certficate Request Info
            X500Name x500Name = new X500Name(certificateDN);
            byte[] certReqInfo = createCertificationRequestInfo(x500Name, keypair.getPublic());

            // generate Signature over Certficate Request Info
            String algorithm = "SHA1WithRSA";
            Signature signature = Signature.getInstance(algorithm);
            signature.initSign(keypair.getPrivate());
            signature.update(certReqInfo);
            byte[] certReqInfoSignature = signature.sign();

            // create PKCS#10 Certificate Signing Request (CSR)
            byte[] csrDEREncoded = createCertificationRequestValue(certReqInfo, algorithm, certReqInfoSignature);
            String csrString = createCSRString(csrDEREncoded);
            String csrPEMEncoded = csrString;

            // write to file
            writeToFile(csrDEREncoded, certificateName + ".der");
            writeToFile(csrPEMEncoded.getBytes(), certificateName + ".pem");
            writeToFile(csrString.getBytes(), certificateName + ".csr");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String certToString(X509Certificate cert, String file) {
        StringWriter sw = new StringWriter();
        try {
            File f = new File(file);
            String fileName = f.getName();
            int index = fileName.lastIndexOf('.');
            String extension = "";
            if(index > 0) {
              extension = fileName.substring(index + 1);
            }
            if("crt".equalsIgnoreCase(extension)){
                sw.write("-----BEGIN CERTIFICATE-----\n");
                sw.write(DatatypeConverter.printBase64Binary(cert.getEncoded()).replaceAll("(.{64})", "$1\n"));
                sw.write("\n-----END CERTIFICATE-----\n");
            }
            try {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                out.println(sw.toString());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (CertificateEncodingException e) {
            e.printStackTrace();
        }
        String out = sw.toString();
        //System.out.println(out);
        return out;
    }
    
    private static String keyToString(KeyPair keyPair, String file) {
        StringWriter sw = new StringWriter();
        try {
            File f = new File(file);
            String fileName = f.getName();
            int index = fileName.lastIndexOf('.');
            String extension = "";
            if(index > 0) {
              extension = fileName.substring(index + 1);
            }
            String m = fileName.split("_")[1].toUpperCase();
            sw.write("----------BEGIN RSA "+m+" KEY----------\n");
//            if(m.equalsIgnoreCase("public")){
//                sw.write(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
//            }else{
//                sw.write(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()));
//            }
            sw.write(Base64.getEncoder().encodeToString(m.equalsIgnoreCase("public")?keyPair.getPublic().getEncoded():keyPair.getPrivate().getEncoded()));
            sw.write("\n-----END RSA "+m+" KEY-----\n");
            
            try {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
                out.println(sw.toString());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String out = sw.toString();
        //System.out.println(out);
        return out;
    }

    private static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048, new SecureRandom());
        KeyPair keypair = keyGen.generateKeyPair();
        return keypair;
    }

    private static String createCSRString(byte[] data) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final PrintStream ps = new PrintStream(out);
        ps.println("-----BEGIN NEW CERTIFICATE REQUEST-----");
        ps.println(Base64.getMimeEncoder().encodeToString(data));
        ps.println("-----END NEW CERTIFICATE REQUEST-----");
        return out.toString();
    }

    private static byte[] createCertificationRequestInfo(X500Name x500Name, PublicKey publicKey) throws IOException {
        final DerOutputStream der1 = new DerOutputStream();
        der1.putInteger(BigInteger.ZERO);
        x500Name.encode(der1);
        der1.write(publicKey.getEncoded());

        // der encoded certificate request info
        final DerOutputStream der2 = new DerOutputStream();
        der2.write((byte) 48, der1);
        return der2.toByteArray();
    }

    private static byte[] createCertificationRequestValue(byte[] certReqInfo, String signAlgo, byte[] signature) throws IOException, NoSuchAlgorithmException {
        final DerOutputStream der1 = new DerOutputStream();
        der1.write(certReqInfo);

        // add signature algorithm identifier, and a digital signature on the certification request information
        AlgorithmId.get(signAlgo).encode(der1);
        der1.putBitString(signature);

        // final DER encoded output
        final DerOutputStream der2 = new DerOutputStream();
        der2.write((byte) 48, der1);
        return der2.toByteArray();
    }

    private void writeToFile(byte[] data, String file) throws FileNotFoundException, IOException {
        String str = destination + "/" + file;
        try (FileOutputStream out = new FileOutputStream(str)) {
            out.write(data);
        }
    }

    /**
     * Save Files
     *
     * @param fileName
     * @param mod
     * @param exp
     * @throws IOException
     */
    private void saveKeys(String fileName, BigInteger mod, BigInteger exp) throws IOException {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            //System.out.println("Generating " + fileName + "...");
            fos = new FileOutputStream(fileName);
            oos = new ObjectOutputStream(new BufferedOutputStream(fos));

            oos.writeObject(mod);
            oos.writeObject(exp);

            //System.out.println(fileName + " generated successfully");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                oos.close();

                if (fos != null) {
                    fos.close();
                }
            }
        }
    }

    private String removeExtension(String in) {
        int p = in.lastIndexOf(".");
        if (p < 0) {
            return in;
        }

        int d = in.lastIndexOf(File.separator);

        if (d < 0 && p == 0) {
            return in;
        }

        if (d >= 0 && d > p) {
            return in;
        }

        return in.substring(0, p);
    }

    private void showMessage() {
        Object[] obj = {"SSL Certificates Generated."};
        Object stringArray[] = {"View Certificates", "OK"};
        if (JOptionPane.showOptionDialog(null, obj, "System Message",
                JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, new ImageIcon(getClass().getResource("/images/info-36.png")), stringArray, obj) == JOptionPane.YES_OPTION) {
            try {
                //open destination folder
                java.awt.Desktop.getDesktop().open(new File(destination));
            } catch (Exception e) {
            }
        } else {
            //System.exit(0);
        }
    }
}
